package test.qa.eric;

import java.util.Scanner;

public class MainClass {
	
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Press 1 for question one and 2 for question two ");
        int question = sc.nextInt();
        if(question ==1 ) {
        	System.out.println("Please enter a number: ");
    		while (!sc.hasNextInt()) {
    		    System.out.println("A number, please?");
    		    sc.next();
    		}
    		int number = sc.nextInt();
    		if(number > 2 && number < 20) {
    			
    		    for(int i = 1; i<10; i++) {
    		        System.out.println(number +" * "+i+" = "+(number*i));   
    		    }
    		    
    		} else {
    			System.out.println("Out of range, number should be between 2 and 20");
    		}
        }else if(question == 2) {
        	System.out.println("Please enter a number: ");
    		while (!sc.hasNextInt()) {
    		    System.out.println("A number, please?");
    		    sc.next();
    		}
    		    int number = sc.nextInt();
                int nextPalindrome = Palindromes.nextPalindrome(number);		    
    		    int nextPrimePalindrome = nextPalindromePrime(nextPalindrome);
    		    System.out.println("The smallest Prime Palindrome after "+number+" is "+nextPrimePalindrome);
        }else {
        	System.out.println("Invalid choice of question");
        }
		
    }
    
    static int nextPalindromePrime(int nextPalindrome) {
    	if (PrimeNumbers.isPrime(nextPalindrome)) {
    		return nextPalindrome;
    	} else {
    		while(!PrimeNumbers.isPrime(nextPalindrome = Palindromes.nextPalindrome(nextPalindrome))) {
    			nextPalindrome = Palindromes.nextPalindrome(nextPalindrome);
            } 
            return nextPalindrome;	
    	} 
    
  }
}
