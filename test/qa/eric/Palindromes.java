package test.qa.eric;

public class Palindromes {
	//check if the input number is a palindrome
	public static boolean IsPalindrome(int num) {
    	String original =  Integer.toString(num);
    	 String reverse = ""; // Objects of String class     
         int length = original.length();   
         for ( int i = length - 1; i >= 0; i-- )  
            reverse = reverse + original.charAt(i);  
         if (original.equals(reverse))  
            return true;
         else  
            return false;   
      }
	
	//find the next palindrome
	public static int nextPalindrome(int number) {
        while(!IsPalindrome(++number)) {	
        } 
        return number;
    }
}
